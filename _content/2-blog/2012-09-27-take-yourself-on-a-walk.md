---
title: Take Yourself on a Walk
author: erin
categories:
  - exercise
excerpt: |
  <p>
  	  Fido needs 2-3 walks a day, but how about you? This principle of daily movement and fresh air is often overlooked when applied to ourselves and yet we are firm believers that dogs shouldn’t be locked up in a kennel all day. If you don’t have a dog, it’s time to put down the remote and start taking yourself on walks. Your brain, body, and boss at work will thank you.
  </p>
_layout: post
---
<p>
	<img src="/assets/img/blog/Man walking fall.jpg" style="width: 325.8144927536232px; height: 217px;">
</p>
<p>
	<span style="line-height: 1.45em;">Fido needs 2-3 walks a day, but how about you? This principle of daily movement and fresh air is often overlooked when applied to ourselves and yet we are firm believers that dogs shouldn’t be locked up in a kennel all day. If you don’t have a dog, it’s time to put down the remote and start taking yourself on walks. Your brain, body, and boss at work will thank you.</span>
</p>
<p>
	  Being glued to a desk all day and neglecting to incorporate movement into the day, is detrimental to productivity in the office in addition to overall health. The brain needs oxygen rich blood and nutrients, and movement increases blood flow to this vital organ, therefore, maximizing the brain’s performance in a work environment. A recent study conducted by the Proceedings of the National Academy of Sciences concluded that walking (and exercise in general) increases the size of your brain’s hippocampus, which consequently improves your memory. Furthermore, research participants who exercised regularly learned faster than their sedentary counterparts.
</p>
<p>
	  Recent studies are also suggesting that walking in nature versus on a treadmill, will amplify your brain’s ability to concentrate. Renowned brain researcher Frances Kuo, PH.D. conducted studies linking a reduction in ADD symptoms with time spent in natural surroundings. Her theory is that being in a natural environment refocuses your brain’s pre-frontal cortex, which is heavily utilized in a typical office setting. The brain’s ability to retain focus is often diminished when a person fixates on a particular mental task for a prolonged period without a mental and physical hiatus from the cubical. A mid-day walk around the block can help.
</p>
<p>
	  Incorporate walks and fresh air into your schedule this week, and watch your productivity and focus increase. Try the following tips:
</p>
<ul>
	<li>Instead of meeting your friend for coffee, catch up while taking a stroll in a local park.</li>
	<li>Before the next time you call a loved one, lace up your sneakers so you can walk around your neighborhood during the phone call.</li>
	<li>When the weather is nice, go outside to eat your lunch in order to get some fresh air.</li>
</ul>