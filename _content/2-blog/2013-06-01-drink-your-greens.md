---
title: 'Drink Your Greens! '
author: erin
categories:
  - Green smoothies
  - Spinach
  - Smoothie
excerpt: |
  <p>
  	  Popeye and your mom have been preaching the same message for years, and it’s a good one.
  </p>
  <p>
  	  Eat more spinach.
  </p>
  <p>
  	  72.8% of Americans eat 2 or less vegetables per day, according to the CDC. It’s time to listen to Popeye and Mom. Vegetables are packed with disease fighting nutrients. Furthermore, most vegetables are very low in calories and sugar, allowing you to fill up on veggies guilt free. Use them as a non-restricted “free” food in your diet. The more the merrier.
  </p>
  <p>
  	  If you have a hard time increasing your daily vegetable intake, I have a solution...
  </p>
_layout: post
---
<p>
	<img src="/assets/img/blog/Popeye spinach.jpg">
</p>
<p>
	  Popeye and your mom have been preaching the same message for years, and it’s a good one.
</p>
<p>
	  Eat more spinach.
</p>
<p>
	  72.8% of Americans eat 2 or less vegetables per day, according to the CDC. It’s time to listen to Popeye and Mom. Vegetables are packed with disease fighting nutrients. Furthermore, most vegetables are very low in calories and sugar, allowing you to fill up on veggies guilt free. Use them as a non-restricted “free” food in your diet. The more the merrier.
</p>
<p>
	  If you have a hard time increasing your daily vegetable intake, I have a solution. Green smoothies, which are fruit based smoothies with a small handful of greens, are a quick, easy, and tasty way to increase your vegetable servings. It may sound unappetizing at first, however, with the right amount of fruit, it tastes like a delicious fruit smoothie rather than grass. You receive about 3x the amount of nutrients by blending the greens because it increases their digestibility and therefore their ability to absorb into your system. For example, 1 cup of fresh spinach in a smoothie is the equivalent of about 3 cups of spinach that are eaten normally. Maximize the nutrient potential of spinach and other leafy greens by making the following power smoothie. Popeye and your mom will be proud.
</p>
<p>
	<strong>Green Power Smoothie:</strong>
</p>
<ul>
	<li>1/4 cup fresh leafy greens or frozen spinach</li>
	<li>1/2 cup frozen strawberries</li>
	<li>1/2 banana</li>
	<li>1 cup of water (if you don't like the smoothie as is, use 1/2 c. water and 1/2 c. apple or orange juice, which will make the smoothie sweeter)</li>
	<li>1 Tbsp of chia (preferably soaked over night), freshly ground flax seed, or melted coconut oil</li>
</ul>
<p>
	  Note: Some individuals struggle to digest raw spinach, if that’s the case, use romaine lettuce or another leafy green.
</p>
<p>
	  Once you become more comfortable with drinking your greens, slightly increase the amount of greens and experiment with different combinations of greens and fruit. Enjoy!
</p>