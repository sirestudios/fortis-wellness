---
title: Corporate Wellness
_layout: container
_template: corporate-wellness
_fieldset: corporate-tabs
page_tagline: Optimize Your Workforce
tabs:
  - 
    tab_name: Overview
    tab_content: |
      <p>
      							<em>Imagine a world in which your employees took fewer sick days, were more productive and used fewer health benefits. This is an employer’s dream due to the consequential savings for the business.</em>
      						</p>
      
      
      
      
      						<p>
      							Times are tough and all employers are seeking effective ways to reduce costs while simultaneously strengthening their business. Luckily, there is a solution. Companies who implement preventative corporate wellness programs reduce health expenses by wisely investing in their greatest assets, their employes. After engaging in Fortis Wellness’ holistic wellness programs, hundreds of employees have seen a marked improvement in their overall health, weight, productivity, energy and morale. Fortis Wellness provides a 12 week Nutrition Workshop Series, Educational Seminars, and Corporate Bootcamps to help strengthen your company. If you live in the greater D.C. area and you believe that your business would benefit from Fortis Wellness’ corporate program, please contact us at <a href="mailto:fortiswellness@gmail.com">FortisWellness@gmail.com</a>.
      						</p>
  - 
    tab_name: 12 Week Workshop
    tab_content: |
      <p>
      							During this 12 week series, employees gather once a week for a one hour interactive workshop, during which various pertinent health topics are discussed. For example, employees will learn how to naturally improve energy, increase mental clarity, prevent and manage diabetes and cardiovascular disease, cut cravings, and create a healthy meal plan on a budget. The program also includes the following support to improve results:
      						</p>
      
      
      
      
      						<ul>
      							<li>Personalized health goals</li>
      							<li>Weekly health challenges </li>
      							<li>Educational handouts </li>
      							<li>A recipe booklet </li>
      							<li>Healthy food and beverage samples at the workshops </li>
      							<li>Weight and body fat percentage tracking using our on-line interactive tracking app </li>
      							<li>A charted analysis of each participants nutritional biological strengths and weaknesses (using Nutri-Q software)</li>
      						</ul>
      
      
      
      
      						<p>
      							After completing the 12 week program, clients are healthier, happier, more productive, and at lower risk for diseases due to the positive lifestyle changes they have made.</p>
  - 
    tab_name: Bootcamp
    tab_content: |
      <p>
      							Wake up. Eat breakfast. Drive to the office. Sit at your desk. Eat lunch. Attend meetings. Commute home. Crash on the sofa to eat dinner and watch TV before falling asleep....repeat. Does this sedentary routine sound familiar?
      						</p>
      
      
      
      
      						<img src="/assets/img/shoes.jpg" width="300" class="wellness" alt="Bootcamp" style="">
      
      
      
      
      						<p>
      							Unfortunately, the average American spends 7.7 hours a day seated and this inactivity negatively affects their health and productivity, according to studies conducted by Harvard Medical School. Corporate fitness programs help to combat the detrimental health effects that result from the typical sedentary work environment. The US Department of Health and Human Services reported that, “Worksites with physical activity programs helped to reduce health care costs of employees by 20-55%, reduce short term sick leave by 6-32%, and increase productivity by 2-52%.” Fortis Wellness provides dynamic fitness programs to strengthen your individual employees and consequently your company at large.
      						</p>
      
      
      
      
      						<p>
      							Join your fellow co-workers in Fortis Wellness’ Corporate Bootcamp and rejuvenate your body with a time efficient functional workout. Each workout is unique, so your body and brain will never be bored. Fortis Wellness’ certified trainers accommodate participants of all fitness levels, by demonstrating and providing exercise variations, therefore, making the workouts challenging yet doable for participants of all fitness levels. If your company does not yet have an on-site gym, our instructors can conduct the workout in an empty conference room, using portable equipment and bodyweight for resistance training. 30 minute , 45 minute, and 60 minute classes are all available. If you believe that you and your colleagues would benefit from the Corporate Bootcamp program, please contact us at to set up classes at your place of employment.</p>
  - 
    tab_name: Seminars
    tab_content: |
      <img src="/assets/img/group.jpg" width="280" class="wellness" alt="group_seminar">
      
      
      
      
      						<p>
      							Educating employees about nutrition, fitness, and stress reduction is a key component in helping them to positively modify their lifestyle habits. Fortis Wellness’ group seminars empower participants with the knowledge and tools they need to succeed.
      						</p>
      
      
      
      
      						<h4>Life in the Fast Lane</h4>
      
      
      
      
      						<p>
      							This educational seminar teaches participants, who have a busy professional and personal life, how to effectively meal plan and eat healthy on the go. During this interactive workshop, employees will also learn how to naturally fight stress and energize their body so that they can live life in the fast lane without having their body’s engine prematurely break down on them.
      						</p>
      
      
      
      
      						<h4>Sane and Slim: Surviving the Holidays</h4>
      
      
      
      
      						<p>
      							Holidays are supposed to be a time of joyful celebration with friends and family; however, they often become stressful events that result in a food coma and a few extra pounds. Luckily, you can enjoy healthy versions of your favorite holiday treats, while avoiding certain popular holiday favorites that further stress the body. Fortify yourself for the holidays by learning not only what sugary and high calorie treats will lead to fatigue, ancient, and weight gain, but also what holiday favorites are on the "safe" list. Your waist line will thank you.
      						</p>
      
      
      
      
      						<h4>Fitness Fuel</h4>
      
      
      
      
      						<p>
      							There are dozens of sports drinks, energy supplements, and protein powders on the market for athletes, and it can be daunting trying to weed through them all and determine what is best for your body. This workshop is tailored to employees who are eager to learn ways to optimize their performance in the gym or on the sports field through nutrition. The seminar will focus on optimal hydration, pre and post workout meals, reducing your recovery time, improving injury healing time, and reducing inflammation.
      						</p>
      
      
      
      
      						<p>
      							<em>*Workshop topics can be tailored for your company. Please contact us for details.</em></p>
  - 
    tab_name: NTP Program
    tab_content: |
      <h4><em>Coming Soon:</em></h4>
      
      
      
      
      
      
      
      
      
      						<p>
      							Attention Nutritional Therapist Practitioners! If you would like to help change the health and culture of corporate America by providing corporate wellness programs to businesses in your area, I encourage you to consider starting your own corporate wellness program, using the Fortis Corporate Wellness Program packet. This twelve week NTP specific program will include a participant booklet with recipes, food journals, worksheets, and weight tracking sheets. Furthermore, as a Corporate Wellness instructor you will receive twelve power point presentations, marketing material, and a comprehensive teacher's manual to guide you through. This program is a great business model for recent graduates who want to help large numbers of Americans but who don't have the capital for expensive office space. The program is almost ready to be released, so please contact us if you are interested in the program.</p>
---
