---
title: Cookie Bites
author: karen
categories:
  - recipes
  - desserts
excerpt: |
  <p>
  	  I’m a foodie and therefore, I’m a big believer in eating delicious food. Although I avoid processed food and refined sugar, occasionally I indulge in a guilt free treat. With these Cookie Bites, I can satisfy my cravings, eat something that doesn’t spike my blood sugar, and still feel pretty good knowing that I am nourishing my body.
  </p>
_layout: post
---
<p>
	  I’m a foodie and therefore, I’m a big believer in eating delicious food. Although I avoid processed food and refined sugar, occasionally I indulge in a guilt free treat. With these Cookie Bites, I can satisfy my cravings, eat something that doesn’t spike my blood sugar, and still feel pretty good knowing that I am nourishing my body.
</p>
<p>
	<span style="line-height: 1.45em;">They are low in carbs and sugars, and high in healthy fats and protein. Added bonus: They taste like peanut butter chocolate chip oatmeal cookie dough. Yes, you heard that right – peanut butter. Chocolate. Oatmeal. COOKIE DOUGH. And to all of the male readers, my husband even thinks they are awesome and gave them the man stamp of approval! You should most definitely give these a try – they are super easy, and you won’t regret it!</span>
</p>
<p>
	  As for the nutritional benefits for this recipe, which I love including, they are as follows:
</p>
<ul>
	<li>Almond meal – high in fiber, protein, and healthy fats</li>
	<li>Chia seeds – Aztec superfood – high in omega-3, fiber, antioxidants, and minerals; some protein</li>
	<li>Pecans – added protein and good fats</li>
	<li>Nut butter – more protein!</li>
	<li>Dark chocolate – antioxidants and magnesium! haha</li>
	<li>Raw honey – antibacterial, soothing, healing properties – another superfood</li>
	<li>Coconut oil – one of THE best superfoods you can eat – high in laurie acid, extremely healthy fat, antioxidant, antibacterial, good for your skin, your gut, your brain…basically, this will make you Superman.</li>
</ul>
<h3>Recipe: PB Chocolate Oatmeal “Cookie Dough” Bites</h3>
<p>
	  These are great because they are super-versatile – switch out any dry ingredient (beside the chia seeds) for a different one, and any wet ingredient for a similar one. The possibilities are endless!
</p>
<p>
	  Ingredients:
</p>
<ul>
	<li>1 cup gluten-free rolled oats (I use Bob’s Red Mill)</li>
	<li>1/2 cup almond meal</li>
	<li>1/2 cup chopped pecans (you can substitute 1 cup of another dry</li>
	<li>ingredient in the place of the almond meal and pecans)</li>
	<li>1/2 cup ground chia seeds</li>
	<li>dash Himalayan or Celtic sea salt</li>
	<li>1/2 cup dark chocolate chips/chunks</li>
	<li>1/2 cup organic/all-natural peanut butter (almond or some other nut</li>
	<li>butter would be a healthier option, but I’m a sucker for chocolate and peanut butter!)</li>
	<li>2 Tablespoons raw honey (add more if you want them sweeter)</li>
	3-4 drops liquid stevia
	<li>1/4 cup melted coconut oil</li>
</ul>
<h3>Directions</h3>
<p>
	  In a medium bowl, combine all dry ingredients and stir well.Add wet ingredients and mix until well-incorporated. Texture should be similar to raw cookie dough. If it’s not, add more almond meal or honey/coconut oil.
</p>
<p>
	  Place in refrigerator to harden – approximately 30 minutes. Remove from refrigerator and roll into small balls. Place in an airtight container and place back in fridge to re-harden. Store in fridge and enjoy anytime you need a little burst of energy, or to satisfy that sweet tooth without destroying your health!
</p>