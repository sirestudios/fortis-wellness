---
title: About
_layout: container
_fieldset: page
page_tagline: Meet Fortis Wellness
_template: ""
---
<h3>Our Mission</h3>
<p>
	    The mission of Fortis Wellness is to affect positive change in the health of employees through preventative nutrition and fitness programs. The word "fortis" is Latin for strong, courageous, and powerful. This word embodies the company's mission of strengthening individuals and businesses by educating and motivating clients, giving them the courage to reach beyond mediocrity and be the best and healthiest version of themselves.
</p>
<p>
	<img src="/assets/img/profile-erin.png" class="profile" style="width: 110.43421052631578px; height: 154px;">
</p>
<h3>Erin Mulhern</h3>
<p>
	<strong>Founder and Owner</strong>
</p>
<p>
	     Health and wellness have always been a passion of Erin’s and she feels incredibly blessed to share that with others. From a young age, Erin realized that eating healthy and cross training made a significant difference in her athletic performance and even in her mental focus at school. Desiring to have a competitive edge over her athletic opponents, she took the time to research and implement all that she learned about health into her lifestyle.
</p>
<p>
	    Hoping to affect change in health policy in her career, Erin studied Political Science in college where she received her Bachelor’s degree. She then continued her studies in nutrition with the Nutritional Therapy Association, where she became board certified as a Nutritional Therapist Practitioner. After working in health policy in our nation’s capital, it soon became apparent that the most effective way to help others improve their health was a more grassroots approach of working with individuals and groups.
</p>
<p>
	    Erin became a Personal Trainer and started using that skill set in conjunction with her Nutritional Training to help clients create a personalized exercise program that compliments and enhances their nutritional profile. Throughout the last six years, she has used this integrated approach to well-being with her clients whether she was giving an individual consultation in her office, presenting at a corporate event, or training athletes on the football field.
</p>
<p>
	    Erin has experience with numerous facets of the wellness field due to her work with a variety of clients, including nationally ranked athletes, businessmen, stay at home moms, elderly, and youth. Each client and group is unique, as are the challenges that accompany working with them. However, the reward of witnessing the positive changes that occur in her clients are priceless for Erin. She is thankful to work in such a rewarding field.
</p>
<p>
	<img src="/assets/img/profile-karen.png" class="profile">
</p>
<h3>Karen Kempf</h3>
<p>
	<strong>Chef</strong>
</p>
<p>
	     Karen Kempf is a Certified Natural Chef, specializing in therapeutic and allergen-free cooking. After struggling with many of her own health problems and dietary restrictions during college, she was inspired to dedicate herself to helping others in the same situation. Her passion is now informing others about the health benefits of a whole foods diet, and equipping them with the cooking skills to easily make this lifestyle adjustment, without sacrificing flavor and aesthetic appeal. Her favorite part of the job is seeing the hope in her clients’ eyes as they begin to realize they can, indeed, be well again and enjoy their food.
</p>
<p>
	<img src="/assets/img/veronicafortis.png" class="profile">
</p>
<h3>Veronica Thomas</h3>
<p>
	<strong>Administrative Assistant</strong>
</p>
<p>
	     Veronica Thomas’ love and passion for food started as soon as she was tall enough to see over the kitchen counter. From then on, it was difficult to pull her away from the kitchen. She enjoys every aspect of nutrition and wellness, and loves keeping others updated with healthy tips. Her active lifestyle includes competitive dance, yoga, and Pilates. Veronica loves furthering her knowledge in health and wellness and sharing that information with others.
</p>
<p>
	<img src="/assets/img/mariafortis2.png" class="profile">
</p>
<h3>Maria Guyant</h3>
<p>
	<strong>Administrative Assistant</strong>
</p>
<p>
	     Maria Guyant is a nutrition advocate and a fitness enthusiast who has been passionate about both subjects for many years.  She has spent much time researching and learning about nutrition and fitness and enjoys helping others become aware of the right things to eat and the right ways to exercise. She implements all of her knowledge about health into her everyday life through her healthy lifestyle choices. She hopes to be an inspiration to others to take control of their wellness.
</p>