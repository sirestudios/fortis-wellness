$(function(){
	$body   = $('body');
	$header = $body.find('.global-header');
	$carousel = $body.find('.carousel');

	if ( !!$carousel.length ){
		$carousel.carousel({
			interval: 8000
		});
	}

	$(window).on('scroll', function(e){
		if ($(document).scrollTop() > 30)
			$header.addClass('collapse');
		else
			$header.removeClass('collapse');
	});
});