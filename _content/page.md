---
title: Home
_layout: home
_fieldset: home
_template: home
video_slide: Show
featured_slide: Show
header_slideshow:
  -
    background: /assets/img/home/home-fruit.jpg
    title: 'Take control of your health:'
    caption: |
      Schedule a nutrition consultation and learn
      how to properly fuel your unique body.
    link: >
      http://fortiswellness.com/individual-consultations
  -
    background: /assets/img/home/home-field.jpg
    title: Optimize Your Workforce.
    caption: |
      Our Corporate Wellness program will help
      increase employees’ energy and overall health.
    link: >
      http://fortiswellness.com/corporate-wellness
quotes:
  -
    quote: |
      <p>
      	"Van Metre’s Corporate Wellness Program has been a huge success! For the past 2 1/2 years, Erin Mulhern has been leading our weekly Nutritional Lunch and Learn Workshops and the results have been fantastic...Consequently, Van Metre saw an annual $200,000 savings in health care costs as noted in the <a href="http://www.bizjournals.com/washington/print-edition/2012/09/21/wellness-creates-savings-of-200k.html?page=all">following article, which highlights our program.</a>
      </p>
    source: >
      Rick Rabil, CEO and President of Van
      Metre Companies
  -
    quote: |
      <p>
      	"For years I’ve had problems losing weight. After only 3 months on the nutritional plan made for me by Erin Mulhern, my whole body changed for the better and I feel great. My weight is down more than 30lbs, and it is continuing to drop. For the first time in years, things are looking up!
      </p>
    source: Betsy Ryan
  -
    quote: |
      <p>
      	"I love extreme bike riding and the Corporate Wellness nutrition class has given me the nutrition knowledge to enhance that passion in my life. By adding good nutrition to my current exercise routine, I lost 18 lbs and 4.5% body fat during Erin’s 12 week program! Thanks!
      </p>
    source: Mr. Stacy Edward
---
<p>
	Fortis Wellness is dedicated to help strengthen you and your company through preventative based nutrition and fitness programs.  Increase productivity and energy in the office and reduce your company’s bottomline health care costs. Join the Corporate Wellness revolution that is starting to sweep across America and witness the positive changes that result from investing in your health and the health of your employees.
</p>
