---
title: Foodies and Football Fanatics
author: erin
categories:
  - recipes
  - football
excerpt: |
  <p>
  	  Let’s be honest, many of us enjoy Super Bowl Sunday primarily for the nachos, wings, hot dogs, chips and dip, deserts, beer, and of course, the commercials. Unfortunately, we often leave the party feeling a bit heavier than when we came in due to the processed food brick that our stomach is struggling to digest. I want you to enjoy Super Bowl without the subsequent junk food hangover or guilt...
  </p>
_layout: post
---
<p>
	  Let’s be honest, many of us enjoy Super Bowl Sunday primarily for the nachos, wings, hot dogs, chips and dip, deserts, beer, and of course, the commercials. Unfortunately, we often leave the party feeling a bit heavier than when we came in due to the processed food brick that our stomach is struggling to digest. I want you to enjoy Super Bowl without the subsequent junk food hangover or guilt of eating Cheese-wiz; so before you throw your nutritional New Year’s resolutions to the wayside, try some healthy party food alternatives and simultaneously impress your guests. I am foodie and a sports fan, and I truly believe that the two passions can live in harmony this upcoming weekend.
</p>
<h3>Nachos Sanos (Healthy Nachos)</h3>
<p>
	  Attention Nacho lovers: There is nothing healthy or natural about Cheese-wiz, heat-and-serve dips or most packaged chip dips. These dipping concoctions typically contain processed fats, preservatives, chemicals, and unhealthy additives, hence the unpleasant feeling in your stomach as you leave the party. Here's a delicious alternative that takes 10 min. to prepare (unless you choose to do lots of extra chopping for more toppings).
</p>
<ul>
	<li>Preheat the oven to 375</li>
	<li>Bean dip (to be put on top of the chips or kept on the side for dunking)</li>
	<li>1 can re-fried beans (Trader Joe's organic, canned)</li>
	<li>1/2 block cream cheese (4 oz., organic is good, raw is best)</li>
	<li>1/2-2/3 cup salsa (whatever you like; I use Costco's Kirkland organic--it's like a gallon for $5!)</li>
	<li>Mix the above over low heat in a small pot</li>
	<li>Line an edged baking pan (the biggest one you have) with parchment paper</li>
	<li>Fill the pan full of tortilla chips (I used organic Kirkland brand)</li>
	<li>Put shredded cheddar (I use Trader Joe's raw milk cheddar) over the chips</li>
	<li>Add your favorite ingredients: chopped tomatoes, onions, peppers, jalapenos, drops of hot sauce, olives, black beans, more salsa, shredded chicken, etc.</li>
	<li>Add the bean dip</li>
	<li>Stick the whole thing in the oven for 5 min. or so until the cheese bubbles and is "nacho-y"</li>
	<li>Add cold/warm temp toppings like sour cream, guacamole (just smash a fresh avocado), or lettuce</li>
</ul>
<p>
	  Enjoy!
</p>
<p>
	<em>Diet modifications--It's already wheat/gluten free if your chips are 100% corn. It's easy to do this dairy free--just remove all the cheese/cream parts and it still tastes amazing! If you want more protein, pile on the beans and add chicken.</em>
</p>
<h3>Rubbed Wings &amp; Sticks</h3>
<p>
	  Oftentimes prepared sauces &amp; marinades have high fructose corn syrup, flavor enhancers &amp; tenderizers (like MSG), preservatives, more sugar, bad salts, and bad oils. Luckily, wings can be healthy and delicious. Look for a sauce in the refrigerated section to avoid preservatives or try local bbq sauces with fewer strange ingredients. However, if you’re feeling adventurous and would like to hear complements on your amazing homemade wing sauce, stick to this healthy rub recipe. You may want to double the recipe, depending on the size of your guest list and the appetites of your guests.
</p>
<ul>
	<li>2 Tablespoons melted butter (Yes, butter is healthier than margarine, which contains processed, unhealthy fats.)</li>
	<li>3 cloves garlic pressed</li>
	<li>2 teaspoons chili powder [add more chili or add some cayenne pepper if you like it to be spicy!</li>
	<li>1 teaspoon garlic powder</li>
	<li>Sea salt (which contains healthy minerals vs. the nutrient void table salt) &amp; pepper to taste</li>
	<li>10 chicken wings/drumsticks</li>
	<li>Put all of it in a large plastic bag and shake to coat</li>
	<li>375 for 1 hour</li>
</ul>
<h3>Chips &amp; Herb Dip</h3>
<p>
	  This is SO easy and addictively delicious (I'm not kidding). It ends up tasting similar to ranch dressing except it’s 100x better for you.
</p>
<ul>
	<li>8oz. cream cheese (pre-whipped is easy, organic is good, raw is best)</li>
	<li>1 t. rosemary (I crush mine up a little first)</li>
	<li>1/s t. thyme</li>
	<li>1/4 t. garlic powder</li>
	<li>1/4 t. freshly ground pepper</li>
	<li>Mix it all with the cream cheese and serve OR refrigerate for up to a day or so</li>
	for an even better taste.
	<li>Serve with whole grain crackers, pita pieces, pita chips, sliced bread, or</li>
	whatever [you could even pre-spread the crackers, add smoked salmon on top, and serve plated like an appetizer]
	<li>Leftover ideas: IF you have any dip leftover (and that's a big "if"), here are</li>
	some ideas:
	<li>eat it sneakily before anyone else discovers it</li>
	<li>put it in an omelet (it sounds weird but it works)</li>
	<li>put it on a sandwich as a delicious spread with leftover meat or deli meat</li>
	<li>put it on a slice of whole grain bread (try Ezekiel) with a fried egg on top</li>
	<li>put it on salmon and warm it up</li>
</ul>
<p>
	  This is my "I only have 3 min. to make something for that party" go-to recipe. :) It would work well with fresh herbs too--just adjust the amounts a bit to taste
</p>
<h3>Other ideas:</h3>
<ul>
	<li>Homemade chili</li>
	<li>Homemade meatballs with a good-quality bbq sauce in a crock-pot</li>
	<li>Popcorn (make it the old way, not in a microwave, and add your own butter and salt)</li>
	<li>Potato skins with real bacon and cheese and onions on top--DON'T use bacon bits, esp. the unnaturally red, hard ones--words can't express how sad it is that someone invented such a culinary travesty</li>
	<li>Cold veggies &amp; hummus or a homemade dip. You could use that cream cheese herb recipe above but replace the cream cheese with sour cream!</li>
	<li>Quesadillas from scratch</li>
	<li>What others can you think of??</li>
</ul>