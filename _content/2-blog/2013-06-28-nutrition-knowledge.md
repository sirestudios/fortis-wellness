---
title: 'NUTrition Knowledge '
author: erin
categories:
  - Nuts
  - Soaking
  - Sprouting
  - Almonds
  - Walnuts
  - Omega 3s
excerpt: |
  <p>
  	<img src="/assets/img/blog/Walnuts.jpg" style="width: 349.3333333333333px; height: 262px;">
  </p>
  <p>
  	  Roasted, salted, raw, candied, sprouted, soaked, shelled, and spiced. When it comes to nuts, it seems as if there are endless varieties to choose from.  However, <strong>what is the best way to consume nuts in order to maximize this power food’s life giving nutrients?</strong>
  </p>
  <p>
  	  First, let’s explore the benefits of consuming nuts. In general, nuts are a source of healthy fats and protein, which provide the body with prolonged energy.  This makes them the perfect snack wether you’re in between meetings at the office or you’re cheering on your favorite baseball team at the stadium. Furthermore, the various nutrients and antioxidants...
  </p>
_layout: post
---
<p>
	<span style="line-height: 1.45em;"> </span>
</p>
<p>
	<img src="/assets/img/blog/Walnuts-20130628155239.jpg" style="width: 369.08378378378376px; height: 277px;">
</p>
<p>
	  Roasted, salted, raw, candied, sprouted, soaked, shelled, and spiced. When it comes to nuts, it seems as if there are endless varieties to choose from.  However, <strong>what is the best way to consume nuts in order to maximize this power food’s life giving nutrients?</strong>
</p>
<p>
	  First, let’s explore the benefits of consuming nuts. In general, nuts are a source of healthy fats and protein, which provide the body with prolonged energy.  This makes them the perfect snack wether you’re in between meetings at the office or you’re cheering on your favorite baseball team at the stadium. Furthermore, the various nutrients and antioxidants in nuts help to lower the risk of cardiovascular disease by decreasing vascular inflammation caused by refined carbs, sugar, and toxins. Lastly, during the digestion of heathy fats, such as nuts, a satiation hormone is triggered, which signals your brain to control portions. Note that your will power needs to work in conjunction with these hormonal triggers of satiation. One or two small handfuls of nuts is plenty.
</p>
<p>
	  Although there are some general health benefits of nuts, each type provides the body with very specific health benefits.
</p>
<ul>
	<li><span style="line-height: 1.45em;">If you are looking for a great source of calcium, magnesium and phosphorus to help build strong bones, the almond would be your best option.  One ounce of almonds has the same amount of calcium as a quarter cup of milk.  </span></li>
	<li><span style="line-height: 1.45em;">Walnuts are are packed with anti-inflammatory omega-3 fatty acids. These omega-3s are great brain food. Ironically, a walnut looks like a tiny brain! </span></li>
	<li><span style="line-height: 1.45em;">If you would like to reduce oxidative damage caused by free radicals, add Brazilian nuts into the mix.  Two of these large nuts are packed with the entire days’ worth of the antioxidant selenium.  </span></li>
	<li><span style="line-height: 1.45em;">Pecans are another antioxidant powerhouse, ranking as one of the top 15 most anti-oxidant rich foods. They also contain more than 19 vitamins and minerals, including folic acid, calcium, magnesium, phosphorus, zinc, potassium, and vitamins A, B, and E.</span></li>
	<li><span style="line-height: 1.45em;">Cashews are loaded with copper and other minerals.  Stress depletes the body of minerals; therefore, snack on cashews and other mineral rich food the next time work stress escalates.</span></li>
</ul>
<p>
	  Every nut plays an important role in making your body healthy, so feel free to mix up the ones you are eating to take advantage of their benefits!
</p>
<p>
	  There is a good, better, best hierarchy when it comes to nut quality and preperation.
</p>
<ul>
	<li><em>Good:</em><span style="line-height: 1.45em;"> Try to avoid dry-roasted, chocolate dipped, cinnamon sugar coated, or nuts salted with table salt.  Roasting damages the fragile fats in nuts, therefore, it’s best to go for raw nuts. The overconsumption of sugar is a catalyst for most degenerative diseases and it should be used only as a rare treat. Table salt has been stripped of many vital minerals that help to balance sodium. Although it would be ideal for you to adhere to these recommendations, remember that it is still better to eat roasted, sweetened, or salted nuts than to grab for Doritos. </span></li>
	<li><em>Better:</em><span style="line-height: 1.45em;"> The next best option is to select raw nuts.</span></li>
	<li><em>Best:</em><span style="line-height: 1.45em;"> Ideally, we should soak or sprout our nuts in order to improve their digestibility and the assimilation of the nutrients. Phytic acid is a component found in nuts and seeds, which impedes our absorption of the precious minerals in nuts. Soaking or sprouting nuts enables the body to assimilate these nutrients properly. You can often buy soaked nuts through your local CSA or you can purchase them on-line at </span><a href="http://www.betterthanroasted.com"></a><a href="http://www.betterthanroasted.com/">http://www.bluemountainorganics.com/betterthanroasted/</a><span style="line-height: 1.45em;"> If you’d like to save a few bucks on buying pre-soaked nuts, you can do your own soaking.</span></li>
</ul>
<p>
	  Here are some simple steps to follow for soaking your nuts:
</p>
<ol>
	<li>Place your choice of nuts or seeds in a bowl with enough filtered water to cover them completely.  Add 1 tbsp of sea salt.  </li>
	<li>Soak your nuts for 6-12 hours, depending on the type of nut.  Generally, the harder the nut, the longer it needs to be soaked.  </li>
	<li>When time is up, thoroughly rinse the nuts.  </li>
	<li>Store in the refrigerator in an airtight container. A paper towel in the base of the container helps to soak up any excess liquid. Soaked nuts and seeds will last in the refrigerator for about a week. <br>
	</li>
</ol>
<p>
	  Final Tips:
</p>
<p>
	  When buying nuts in the shell, be sure to look for clean and un-cracked shells.  Another good indicator of a fresh nut would be to evaluate its smell.  If it is unpleasant, the nuts could very well go rancid. If possible, look for nuts that are sprouted.
</p>
<p>
	  If you’re ready to increase your NUTrition, begin incorporating a variety of nuts into your diet. Enjoy!
</p>