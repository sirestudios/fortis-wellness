---
_fieldset: testimonials
title: Testimonials
_template: testimonials
page_tagline: What Our Clients Say
quotes:
  - 
    quote: |
      <p>
      	<img src="http://fortiswellness.com/assets/img/rick.png" class="profile" alt="Rick Rabi">“Van Metre’s Corporate Wellness Program has been a huge success! For the past 2 1/2 years, Erin Mulhern has been leading our weekly Nutritional Lunch and Learn Workshops and the results have been fantastic. Employees who go through her 12 week program have much more energy, think clearer, and have lost significant amounts of weight. Furthermore, many of them have been able to reduce or eliminate their medications (with doctor approval) due to the amazing internal changes their bodies underwent once they address certain physiological imbalances naturally with nutrition. Consequently, Van Metre saw an annual $200,000 savings in health care costs as noted in the <a href="http://www.bizjournals.com/washington/print-edition/2012/09/21/wellness-creates-savings-of-200k.html?page=all">following article, which highlights our program.</a>Erin Mulhern, thanks again for your significant contribution to our company’s success by making our employees more productive and energized!”
      </p>
    source: >
      Rick Rabi, CEO and President of Van
      Metre Companies
  - 
    quote: |
      <p>
      	“Since participating in the Corporate Wellness program, I’m concentrating better at work.”
      </p>
    source: Janiece Janson
  - 
    quote: |
      <p>
      	 <img src="http://fortiswellness.com/assets/img/debbie.png" alt="Debbie Freivald">“Erin Mulhern’s nutritional program and expertise restored my energy after gall bladder removal and greatly improved my sleep. Furthermore, I’m a late blooming triathlon fanatic and Erin’s unconventional dietary suggestions fueled me to my first half and full distance Ironman finishes. Thank you, Erin for helping me to perform at such a peak level of fitness!”
      </p>
    source: Debbie Freivald
  - 
    quote: |
      <p>
      	“I love extreme bike riding and the Corporate Wellness nutrition class has given me the nutrition knowledge to enhance that passion in my life. By adding good nutrition to my current exercise routine, I lost 18 lbs and 4.5% body fat during Erin’s 12 week program! Thanks!”
      </p>
    source: Mr. Stacy Edward
  - 
    quote: |
      <p>
      	“For years I’ve had problems losing weight. After only 3 months on the nutritional plan made for me by Erin Mulhern, my whole body changed for the better and I feel great. My weight is down more than 30lbs, and it is continuing to drop. For the first time in years, things are looking up!”
      </p>
    source: Betsy Ryan
  - 
    quote: |
      <p>
      	“I’ve struggled with digestive issues all my life and they worsened after having my second daughter. After seeing many doctors that kept prescribing me antibiotics and a specialist that finally diagnosed me with IBS (Irritable Bowel Syndrome), I decided to seek other ways to heal myself. I looked into herbal remedies and nutritionists. Both were costly and a little inconvenient. However, I couldn’t afford to be sick anymore. Then something wonderful happened; I met with Fortis Wellness’ Nutritional Therapist Practitioner, Erin Mulhern, who provided individual nutritional consultations. The “Nutrition Tri-Pack” was definitely worth every penny.
      </p>
      <p>
      	“I saw almost immediate results after Erin put me on an individually tailored diet, which enabled my body to heal naturally without any medication. Furthermore, she taught me how to regain energy through certain foods and through lifestyle changes, which helped heal my tired adrenal glands.
      </p>
      <p>
      	“I have so much more energy during the day and sleep better at night. Erin proved the fact that ‘you are what you eat.’ Thanks to Fortis Wellness, I feel better than I have in years.”
      </p>
    source: E. Mullins, Alexandria, Va.
---
