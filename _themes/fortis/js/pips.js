;(function($, window , document, undefined) {

	// plugin constructor
	var Plugin = function(elem){
		this.elem = elem;
		this.$elem = $(elem);
		this.$items = this.$elem.find( '.carousel-inner' );
		this.$pips = this.$elem.find( '.pips a' );
	};

	// plugin prototype
	Plugin.prototype = {

		init: function() {
			this.bindEvents();
			return this;
		},

		bindEvents: function() {
			this.$elem.on('slid', $.proxy(this.update, this));
			this.$pips.on('click', $.proxy(this.goto, this));
		},

		update: function() {
			var index = this.$elem.find( 'li.active' ).prevAll().length;
			this.$pips.removeClass( 'active' ).eq(index).addClass( 'active' );
		},

		goto: function(e) {
			var index = $(e.target).attr( 'data-index' ) - 1;
			
			this.$elem.carousel(index);

			e.preventDefault();
		}
		
	};

	$.fn.pips = function(target) {
		$(this).carousel({
			interval: false,
			pause: 'hover'
		});

		new Plugin(this).init();
	};

	$( '.carousel' ).pips();

})(jQuery, window , document);

