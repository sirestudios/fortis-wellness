---
title: Get Active at the Office
author: erin
categories:
  - corporate wellness
  - exercise
  - fitness
  - movement
  - stretching
excerpt: |
  <p>
  	  Do you want to amp up your productivity at the office? It's time to get moving. <br>
  	  Movement increases your circulation and consequently the amount of oxygen that travels to your brain. Oxygen is key for good mental focus. Therefore, if you have a sedentary desk job, I encourage you to schedule time during the day to get your blood pumping.
  </p>
  <p>
  	  Implement the following ideas and watch your productivity increase:
  </p>
  <p>
  	  ...
  </p>
_layout: post
---
<p>
	<span style="line-height: 1.45em;"></span>
</p>
<p>
	<img src="/assets/img/blog/Corporate wellness mix.png" style="width: 282.3222748815166px; height: 230px;"><br>
	<span style="line-height: 1.45em;">Do you want to amp up your productivity at the office? <br>
	<br>
	  It's time to get moving. </span>
</p>
<p>
	  Movement increases your circulation and consequently the amount of oxygen that travels to your brain. Oxygen is key for good mental focus. Therefore, if you have a sedentary desk job, I encourage you to schedule time during the day to get your blood pumping.
</p>
<p>
	  Implement the following ideas and watch your productivity increase:
</p>
<ul>
	<li><span style="line-height: 1.45em;">Park your car far away from the entrance (unless you're late, of course!)</span></li>
	<li><span style="line-height: 1.45em;">Bypass the elevator and climb the stairs</span></li>
	<li><span style="line-height: 1.45em;">Request a standing work station</span></li>
	<li><span style="line-height: 1.45em;">Use an upright chair without armrests, which will force you to sit up and engage core muscles</span></li>
	<li><span style="line-height: 1.45em;">Trade out your chair for an exercise ball </span></li>
	<li>Roll out your neck and shoulders at the top of each hour</li>
	<li>Take a short walk during your lunch break</li>
	<li>Jog the stairs instead of grabbing another cup of coffee</li>
</ul>