---
title: Individual Consultations
_layout: container
_template: individual-consultations
_fieldset: consultation-tabs
page_tagline: One on One Consultations
tabs:
  - 
    tab_name: Nutrition Consultations
    tab_content: |
      <img src="{{ theme:img src='betsy.jpg' }}" class="wellness" alt="erin-mulhern-with-client" style="">
      
      
      
      
      		<p>
      			What health burden is weighing you down? Those who suffer from constant fatigue, excess weight, anxiety, mental sluggishness, a blood sugar imbalance, or some other taxing health problem will greatly benefit from nutritional guidance. Athletes can also benefit from a unique diet to maximize their endurance, energy, strength and reaction time. If you’re generally in good health, consider a yearly or bi-annual detox as preventative care. By taking care of your body now, you will reap the benefits of long term health and vitality.
      		</p>
      
      
      
      
      		<p>
      			Your Nutritional Practitioner will treat you as an individual by assessing and supporting your unique nutritional strengths and weakness, helping you to achieve your health goals. Rather than putting a Band-Aid on your symptoms, the root of your health imbalance will be targeted by addressing your body’s needs from the ground up, consequently, giving you long term results. If you’re ready to feel your best, it is time to take control of your health foundationally by investing in yourself and taking advantage of Fortis Wellness’ customized Nutrition Consultations.
      		</p>
      
      
      
      
      		<h4>Nutrition Consultation Investment:</h4>
      
      
      
      
      		<ul>
      			<li>Tri-Pack: $310 for one 90 minute initial evaluation and two 60 minute follow ups</li>
      			<li>Small Tri-Pack: $250 for one 90 minute initial evaluation and two 30 minute follow ups</li>
      			<li>Six-Pack: $440 for one 90 minute initial evaluation, one 60 minute follow up, and four 30 minute follow ups.&nbsp;</li><li>Initial 90 minute Evaluation: $150</li>
      			<li>60 minute Follow Up: $90</li>
      			<li>30 minute Follow Up: $50</li>
      			<li>Follow up e-mail support: $25 per e-mail that lasts up to 30 minutes to respond to</li>
      		</ul>
      
      
      
      
      		<p>Consultations are held at Fortis Wellness' Springfield office.</p><blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><p><span style="background-color: rgb(255, 255, 255);"><span style="color: #000000;"><b><i>7001 Loisdale Drive<br></i></b><b style="line-height: 1.45em;"><i><span style="line-height: 1.45em;">Suite C<br></span></i></b><b style="line-height: 1.45em;"><i><span style="line-height: 1.45em;">Springfield, VA 22150</span></i></b></span></span></p></blockquote><p><span style="line-height: 1.45em;">Do you live far away? You can still get nutritional support from us! Fortis Wellness conducts consultations via video Skype or phone, making them a convenient option for clients throughout the country.</span></p>
  - 
    tab_name: Culinary Consultations
    tab_content: |
      <p>
      			Internal strengthening and healing starts in the kitchen with delicious properly prepared food. Fortis Wellness’ Chef is here to teach you the necessary cooking skills that so many Americans never received or perfected. Learn how to cook nourishing, flavorful meals that will rejuvenate your body and increase your energy and overall health. Furthermore, if you have any specific dietary needs, as determined by your Nutritional Therapist Practitioner, your cooking lesson will be tailored to those nutritional guidelines.
      		</p>
      
      
      
      
      		<img src="{{ theme:img src='chef-karen.jpg' }}" class="wellness" alt="culinary_chef_karen">
      
      
      
      
      		<p>
      			Your video Skype cooking lesson package will begin with an initial phone consultation where our Chef will learn about all of your food preferences, your level of cooking skills, what health issues you are dealing with, and any dietary recommendations you have received. Prior to the cooking lesson, Fortis Wellness’ Chef will e-mail you recipe(s) for your approval, along with a shopping list, so you will have everything on hand for the lesson. You will then receive a one hour cooking lesson over video Skype. This will include learning one or two recipes, based on your dietary needs and taste preferences. Furthermore, you will be taught basic cooking skills (if needed) and tricks to make your new dietary lifestyle easier and more delicious. Empower your self with the skills needed to heal your body in the kitchen. The more you learn, the broader your culinary horizons will become!
      		</p>
      
      
      
      
      		<h4>Culinary Consultation Investment:</h4>
      
      
      
      
      		<ul>
      			<li>Initial consultation and cooking lesson: $100,</li>
      			<li>Subsequent 1 hr cooking lessons $50 and $15 every 30 mins over, for long/complicated lessons.</li></ul>
---
