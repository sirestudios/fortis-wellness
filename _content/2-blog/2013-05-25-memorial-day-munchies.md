---
title: Memorial Day Munchies
author: erin
categories:
  - recipes
  - memorial day weekend
  - memorial day
  - healthy bbq foods
excerpt: |
  <p>
  	<img src="/assets/img/blog/flag cloud.jpeg">
  </p>
  <p>
  	  This weekend while honoring those who bravely gave their lives defending our country, don’t neglect honoring your body with life giving picnic food.
  </p>
  <p>
  	  Memorial Day weekend is upon us. Soon the aroma of the backyard honeysuckle flowers will be accompanied by smells of BBQ and favorite summertime treats. The typical Memorial Day spread of hotdogs, hamburgers, potato rolls, chips and dip, baked beans, potato salad, and lemonade entice many conscious eaters to throw their hands up and succumb to the temptation. Not you. This weekend, honor your body and chose healthy yet tasty alternatives for your holiday picnic and BBQ food.
  </p>
  <p>
  	<strong>1. Healthy Salad: </strong>Potato salad and many cold pasta salads use...
  </p>
_layout: post
---
<p>
	<span style="line-height: 1.45em;">This weekend, while honoring those who bravely gave their lives defending our country, don’t neglect honoring your body with life giving picnic food. </span>
</p>
<p>
	  Memorial Day weekend is upon us. Soon the aroma of the backyard honeysuckle flowers will be accompanied by smells of BBQ and favorite summertime treats. The typical Memorial Day spread of hotdogs, hamburgers, potato rolls, chips and dip, baked beans, potato salad, and lemonade entice many conscious eaters to throw their hands up and succumb to the temptation. Not you. This weekend, honor your body and chose healthy yet tasty alternatives for your holiday picnic and BBQ food.
</p>
<p>
	<strong>1. Healthy Salad: </strong>Potato salad and many cold pasta salads use mayonnaise or dressings with processed oils in them. Although canola oil and vegetable oil were once touted as healthy fats, scientist now better understand the havoc that these processed oils wreak on the body. Check your mayo and salad dressings and see what fats are used in them. This weekend, try a side dish which utilizes healthy oils. The following vibrant vegetable based salad contains olive oil.
</p>
<p>
	<strong>Healthy Tomato, Onion and Cucumber Salad</strong>
</p>
<p>
	<em>Ingredients:</em>
</p>
<ul>
	<li>6 ripe tomatoes </li>
	<li>½ red onion</li>
	<li>A cucumber</li>
	<li>Fresh cilantro and parsley or other herb of choice</li>
	<li>Red Wine Vinegar</li>
	<li>Sea Salt and black pepper</li>
	<li>2 tablespoons of extra virgin olive oil.</li>
</ul>
<p>
	  Chop veggies to desired size and toss in extra virgin olive oil, a splash of Red Wine Vinegar, sea salt, pepper, and fresh herbs. This kind of salad can be easily modified to one’s liking by adding more or less herbs, seasonings, vegetables, beans, etc.
</p>
<p>
	<strong>2. Healthy Drink: </strong>During weekend BBQs we often grab for the lemonade or sweet tea as a healthy alternative to beer. Although these beverages are alcohol free, they still contain high levels of refined sugar. Sugar directly contributes to weight gain and a host of other health problems. Therefore, it’s best to hydrate with drinks naturally flavored with fruit, honey, or flavorful herbs.
</p>
<p>
	<strong>Melon Water </strong><em>by Chef Marcus</em>
</p>
<p>
	<em>Ingredients:</em>
</p>
<ul>
	<li><span style="line-height: 1.45em;">1 Cantaloupe or Honeydew Melon </span></li>
	<li><span style="line-height: 1.45em;">2Tbsp Raw Local Honey (optional) </span></li>
	<li><span style="line-height: 1.45em;">3qt Water</span></li>
</ul>
<p>
	<em> Directions:</em>
</p>
<p>
	  Clean your melon by removing its skin and seeds. Chop melon small enough to fit in your blender. Add 1qt water, 2Tbsp Honey and puree the melon until smooth. Add melon puree to 1 gallon Mason jar or pitcher. Add 2qt water. Stir and Chill.
</p>
<p>
	<strong>3. Healthy Side:</strong> The classic baked beans are a seemingly nutritious BBQ or picnic side dish; however, the canned versions are packed with sugar. For example, one cup of Bush’s Baked Beans contains 24 grams of sugar, which is the equivalent of about 6 teaspoons of sugar! Furthermore, many of the canned baked beans contain poorly processed oils, chemical flavorings, preservatives, and table salt. Try this smoky, flavorful alternative, which is high in protein.
</p>
<p>
	<strong>No-Bean Baked Bean and Bacon </strong>
</p>
<p>
	<em>Ingredients:</em>
</p>
<ul>
	<li>1 small can of tomato paste.</li>
	<li>1/2 cup apple cider vinegar.</li>
	<li>1 cup water.</li>
	<li>2 tbsp dijon mustard.</li>
	<li>1/4 cup raw honey (optional).</li>
	<li>1 medium white onion.</li>
	<li>4-8 ounces of salt pork (cut into very small cubes).</li>
	<li>8 ounces of chicken (chopped).</li>
	<li>1 tbsp Spanish paprika.</li>
	<li>1 tsp chili powder.</li>
	<li>1 tsp garlic powder.</li>
	<li>2 tsp dehydrated onion.</li>
</ul>
<p>
	<em>Directions</em>
</p>
<ul>
	<li>Brown cubed pork and chicken on medium heat, add onions and stir. Add spices and apple cider vinegar. Stir in the dijon mustard and honey. Stir on medium to high until the bacon and onions are slightly browned.</li>
	<li>Add tomato paste and water, stir.</li>
	<li>Pour into slow cooker. Cook on high for 2 hours or low for 5 hours.</li>
	<li>Use an immersion blender or food processor for just a few pulses to even consistency if desired.</li>
</ul>
<p>
	<strong>4. Healthy Meat:</strong> Hot dogs are the all-American junk food. They contain highly processed meat, a harmful flavoring called MSG, corn syrup (sugar), and sodium nitrite, which is a chemical preservative, flavoring, and coloring that has been linked to cancer. Furthermore, these trashy meats are placed in a highly refined grain roll and are topped with ketchup made with high fructose corn syrup. As an alternative, try a chemical free hot dog or sausage from a nitrite and MSG free brand, such as Applegate or US Wellness Meats (<a href="http://www.grasslandbeef.com/Categories.bok">http://www.grasslandbeef.com/Categories.bok</a>). Here’s another delicious alternative for the grill, which doesn’t even require a bun!
</p>
<p>
	<strong>Beef Shish Kabobs on the Grill with Veggies</strong>
</p>
<p>
	<em>Ingredients:</em>
</p>
<ul>
	<li>1 pound beef sirloin, top sirloin, or filet (you can also use chicken)</li>
	<li>1 medium onion</li>
	<li>1 green pepper</li>
	<li>10 large white mushrooms</li>
	<li>1 red pepper (optional)</li>
	<li>1 teaspoon sea salt</li>
	<li>1/8 teaspoon paprika</li>
	<li>1/8 teaspoon black pepper</li>
	<li>1/8 teaspoon garlic powder</li>
	<li>1/8 teaspoon onion powder</li>
	<li>1 tablespoon olive oil</li>
	<li>1 tablespoon melted butter </li>
</ul>
<p>
	<em>Directions:</em>
</p>
<p>
	  Marinade: combine the salt, sugar, paprika, black pepper, garlic powder and onion powder.
</p>
<p>
	  Cut the steak into 1 inch to 1 1/2 inch cubes. Add the cubes to the marinade bowl. Let it rest for 2 hours in the fridge. Then remove it from the refrigerator and let it come back to room temperature. Let it sit at room temp for about 30 minutes.
</p>
<p>
	  Cut the onion in half and then cut each half into quarters. Cut the green pepper open and clean out the seeds. Cut the pepper into 1 inch squares (or close to square as you can). Half mushrooms if they are large.
</p>
<p>
	  Add meat and veggies on the skewers, alternating ingredients for enhanced flavor.  After assembling the kabobs, brush some extra virgin olive oil and melted butter on the skewered meat and veggies to keep them from sticking.
</p>
<p>
	  If using gas, heat the grill on high for 10 minutes prior to cooking.  If cooking on coals, it is best to use natural hardwood charcoal.  Add enough coals to cook over a high heat for 10 to 15 minutes.
</p>
<p>
	  Place the kabobs on the hot grill directly over the flame or coals.  Keep the lid open since you are cooking on direct heat.  Grill for 10 to 12 minutes, rotating 90 degrees every 4 minutes, until the meat is cooked to desired level.  Remove the kabobs from the grill and let them rest for 3 or 4 minutes before serving.
</p>
<p>
	  ENJOY!
</p>
<p>
	  Recipes from: the Fortis Wellness kitchen, Chef Marcus Munoz, www.paleopot.com, and www.grillingcompanion.com
</p>