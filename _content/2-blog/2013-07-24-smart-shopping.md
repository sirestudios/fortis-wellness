---
title: Smart Shopping
author: erin
categories:
  - Grocery shopping
  - Whole foods
  - Shopping
  - Grocery store
  - buying food
excerpt: |
  <h2><span style="font-size: 15px; line-height: 1.45em;">Rule #1: Work the Edges</span><br>
  <p>
  	  If you think about it, the healthy foods (fresh produce, meat, seafood, and eggs) are found along the edges of the supermarket, whereas the center of the store is like a junk food land-mine. Don't let your health resolutions blow up by spending time meandering through those aisles. Spend the majority of your time shopping along the walls and tread carefully when in the aisles.
  </p>
  <p>
  	<strong>Rule #2: Look High and Low</strong>
  </p>
  <p>
  	  Often the worst foods are at eye level on the shelf. Why? It's because the wealthy processed food companies can afford...
  </p>
  <p>
  	<img src="/assets/img/blog/Grocery Shopping Guy K. (free)-20130724075857.jpg" style="width: 356.82576866764276px; height: 238px;">
  </p>
  </h2>
_layout: post
---
<h2><strong style="color: rgb(34, 34, 34);">Rule #1: Work the Edges</strong></h2>
<p>
	  If you think about it, the healthy foods (fresh produce, meat, seafood, and eggs) are found along the edges of the supermarket, whereas the center of the store is like a junk food land-mine. Don't let your health resolutions blow up by spending time meandering through those aisles. Spend the majority of your time shopping along the walls and tread carefully when in the aisles.
</p>
<p>
	<strong>Rule #2: Look High and Low</strong>
</p>
<p>
	  Often the worst foods are at eye level on the shelf. Why? It's because the wealthy processed food companies can afford prime real estate in the grocery store. The healthier food brands that have fewer additives can't compete with these junk food juggernauts. Look high and low for the healthier stuff. Refer to #1 for the healthiest stuff.
</p>
<p>
	<strong>Rule #3: Packaging = Less Nutrition </strong>
</p>
<p>
	  Eat foods that are minimally processed, or better yet, consume foods that aren't processed at all. For example, eat fresh, raw carrots and hummus rather than Veggie Crisps (vegetable chips cooked in unhealthy oils). Even these processed "health" foods have been stripped of the majority of their nutrients during the cooking, refining, and packaging process. Furthermore, most processed food is packed with sugar. Sugar makes you fat and sick. Get back to the basics of nutrition.
</p>
<p>
	<strong>Rule #4: Fewer Ingredients Means Healthier Food</strong>
</p>
<p>
	  It's important to always read food labels; however, it can be overwhelming to try to decipher what it all means. Generally, if you can't pronounce it, don't eat it. Furthermore, regarding food ingredients, (usually) less is more! Therefore, when comparing two similar products, you are probably better off choosing the one with fewer ingredients that you can actually pronounce.
</p>
<p>
	<strong>Rule #5: Keep an Eye on the Key Players</strong>
</p>
<p>
	  Make sure to check the first few ingredients of a product. The ingredients that are used the most appear first on the label's list, so you want those foods to be healthy. Therefore, check to make sure sugar is not one of the first 3 ingredients. Remember that high fructose corn syrup and any word ending in -ose is sugar. Once again, sugar makes you fat and sick so limit your consumption.
</p>
<p>
	<img src="/assets/img/blog/Grocery Shopping Guy K. (free).jpg" style="width: 465px; height: 310px;">
</p>