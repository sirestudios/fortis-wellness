---
title: 'Be a Thanksgiving Lifeguard & Save Your Drowning Veggies'
author: erin
categories:
  - recipes
  - thanksgiving
excerpt: |
  <p>
  	   Thanksgiving should be a time of celebration with loved ones around a beautiful table of nutritious and delicious food. Unfortunately, many classic holiday comfort foods are packed with additives, carbs, and sugar, leaving you ironically uncomfortable.
  </p>
_layout: post
---
<p>
	   Thanksgiving should be a time of celebration with loved ones around a beautiful table of nutritious and delicious food. Unfortunately, many classic holiday comfort foods are packed with additives, carbs, and sugar, leaving you ironically uncomfortable.
</p>
<p>
	 <img src="/assets/img/blog/green-beans.jpg" style="float: right; margin: 0px 0px 10px 10px;" alt="">
</p>
<p>
	   Let‘s take a quick look at sauces‘ role in giving you a food coma, bloating, indigestion and a few extra pounds. Conventional sauces are often packed with processed fats, un-natural flavorings (Ex. MSG), and sugar. Your body struggles to process this concoction, resulting in a digestive revolt. Therefore, I recommend you try an alternative recipe that is sauce free but equally delicious or make your own sauces from scratch, using healthy ingredients.
</p>
<p>
	   This Thanksgiving rather than drowning your green beans in an unhealthy MSG filled cream of mushroom soup, try the following delicious Green Bean Recipe:
</p>
<h3>Ingredients:</h3>
<ul>
	<li>6 thick slices of hormone &amp; anti-biotic free bacon, chopped</li>
	<li>⅓ cup onions, minced</li>
	<li>1 teaspoon minced garlic</li>
	<li>1 pound green beans</li>
	<li>1 cup water</li>
	<li>⅛ teaspoon sea salt</li>
	<li>1 pinch ground black pepper</li>
</ul>
<h3>Directions:</h3>
<p>
	   Place bacon in a large, deep skillet. Cook over medium high heat until the fat begins to render. Stir in onions and garlic; let cook for 1 minute. Stir in the green beans and water. Steam the beans until the water has evaporated and the beans are tender. Season with sea salt and pepper (to taste), add butter if desired, and serve.
</p>