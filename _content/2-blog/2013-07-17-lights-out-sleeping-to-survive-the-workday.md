---
title: 'Lights Out: Sleeping to Survive the Workday'
author: erin
categories:
  - sleep
  - caffeine
  - bed
  - energy
  - productivity
excerpt: |
  <p>
  	  How did you feel this morning when you woke up? Did you need a jolt from your coffee or Red Bull to get moving or did you feel refreshed and rejuvenated without caffeine?
  </p>
  <p>
  	  Caffeine will give you a temporary boost, but in the long run it actually drains your body's natural energy reserves. If you can't wake up without caffeine, that means you are addicted and that it's time to improve your sleep.Both the quantity and the quality of sleep that you receive will greatly impact your energy and productivity at the office.
  </p>
  <p>
  	  Implement the following suggestions in order to improve both your efficiency at work and your overall health.
  </p>
  <ul>
  	<li> Exercising for at least 30 minutes per day can improve your sleep; therefore, include some type of physical activity in your day.  However, if you exercise an hour...</li>
  </ul>
_layout: post
---
<p>
	  How did you feel this morning when you woke up? Did you need a jolt from your coffee or Red Bull to get moving or did you feel refreshed and rejuvenated without caffeine?
</p>
<p>
	  Caffeine will give you a temporary boost, but in the long run it actually drains your body's natural energy reserves. If you can't wake up without caffeine, that means you are addicted and that it's time to improve your sleep. Both the quantity and the quality of sleep that you receive will greatly impact your energy and productivity at the office.
</p>
<p>
	  Implement the following suggestions in order to improve both your efficiency at work and your overall health.
</p>
<ul>
	<li> Exercising for at least 30 minutes per day can improve your sleep; therefore, include some type of physical activity in your day.  However, if you exercise an hour or two before<span style="line-height: 1.45em;"> bedtime, the adrenaline kick you received from your workout and the bright gym lights will signal your body to stay awake for a few more hours, making it difficult to fall asleep. If you have to workout close to bedtime, decrease the intensity of your workout. For example, chose power yoga over spin class.  <br>
	<br>
	</span></li>
	<li>When possible avoid eating desserts and alcohol, which processes as sugar, late in the evening. Ingesting sugary foods and beverages before bed will give you an initial energy kick but it can disrupt your sleep. For example, if you have a blood sugar dip in the middle of the night due to previously ingested sugar, your body will shoot out adrenal hormones that not only normalize your sugar levels but also wake you up. <br>
	<br>
	</li>
	<li> Engage is some type of relaxing activity during the hour before your bedtime. No, not drinking wine. Try stretching, prayer or meditation, reading, journaling, or talking with your family or housemates. These activities will help you to unwind naturally by triggering the hormones that signal it is time for bed. <br>
	<br>
	</li>
	<li>If you want to sleep snug as a bug, it's time to unplug. When the body senses light, it thinks that it is day time. Up until the early 1900's, when the sun set, humans went to bed. Now thanks to electricity, we can artificially extend the daytime light. Unfortunately, when we are around bright lights in the evening, this confuses the body and we continue to produce hormones to keep us awake. Therefore, around 9pm, start to dim the lights in your house and turn off the TV, your ipad, the computer, and your phone. <br>
	</li>
</ul>
<ul>
	<li>Get to bed early. Our body functions best when we go to bed by 10pm or 10:30pm at the latest because our body is programed to detoxify and reboot during those earlier hours of the night. You will generally feel much more refreshed if you go to bed at 10pm and sleep for 8 hours vs. going to bed at 1am and sleep for the same 8 hours. The quality of your sleep matters! <br>
	<br>
	</li>
	<li>Ensure that your bedroom is pitch black. Even the tiniest bit of light in the room can disrupt the hormones that regulate your body's internal clock, which tells the body when it is time to sleep or to be awake. This internal clock is governed by your pineal gland, which produces the hormones to regulate your sleep cycles, specifically melatonin. In order to improve your sleep, close your bedroom door at night, get rid of night-lights, cover your windows, and turn off any blinking electronics. <br>
	<br>
	</li>
	<li>Get 8 hours of quality sleep when possible. The human body is meant to sleep more than the current average of 6 or 7 hours. This summer, take a cue from the sun and start getting ready for bed soon after the sun has set.</li>
</ul>
<p>
	  Sweet dreams!
</p>
<p>
	<img src="/assets/img/blog/Sleeping man.jpg">
</p>