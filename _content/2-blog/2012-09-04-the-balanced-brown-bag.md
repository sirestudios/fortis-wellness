---
title: The Balanced Brown Bag
author: erin
categories:
  - lunch
excerpt: |
  <p>
  	  The alarm clock blares; it is another typical morning trying to get yourself (and your kids) out the door. Amidst the chaos, it is understandably difficult to be creative with packed lunches. Instead of using foods that are loaded with sugar, unhealthy fats, and additives, this week I encourage you to focus on packing a healthy lunch.
  </p>
_layout: post
---
<p>
	  The alarm clock blares; it is another typical morning trying to get yourself (and your kids) out the door. Amidst the chaos, it is understandably difficult to be creative with packed lunches. Instead of using foods that are loaded with sugar, unhealthy fats, and additives, this week I encourage you to focus on packing a healthy lunch.
</p>
<p>
	<span style="line-height: 1.45em;">Think outside the (typical lunch) box and try the following meals and snacks that will give you the energy needed to focus at work and enjoy your day.</span>
</p>
<p>
	  High Fiber Wrap: Chose a whole grain (preferably sprouted) wrap and fill it with a healthy chicken salad or spread hummus on the inside of a wrap before adding turkey, lettuce, tomato, and sprouts. Get creative! The possibilities are endless with wraps. Just make sure you keep the ingredients healthy.
</p>
<p>
	  Fruit and Cheese Plate: Add some sophistication to your lunch or snack by packing fresh fruit, cheese cubes, and whole grain crackers.
</p>
<p>
	  Cold Pasta Salad: Start with a whole grain pasta (preferably sprouted) base and add unprocessed deli meat and cheese. Try a dressing that includes healthy olive oil, like <a href="http://bragg.com/zencart/index.php?main_page=index&amp;cPath=6" target="_blank">Bragg's Healthy Vinaigrette</a>, rather than a less healthy alternative, such as a highly processed vegetable oil based dressing. Don’t forget to sneak in lots of diced vegetables to your healthy pasta salad.
</p>
<p>
	  Green Salad: Start with a base of dark leafy greens, such as romaine lettuce, and pile on the vegetables. Add pulled chicken, turkey, or steak for protein. Find recipes using different combinations of healthy ingredients to keep your taste buds guessing. Variety is the spice of life!
</p>
<p>
	  Pita Pocket: Stuff a whole wheat pita with bell peppers, onions, and cucumbers. A bit of olive oil, feta cheese, and hummus adds healthy protein, fats, and flavor to your culinary creation.
</p>
<p>
	  Nut Butter Crunch: Try topping celery and/or apples with peanut or almond butter and raisins. Add some rolled up deli meat to fill out the lunch.
</p>
<p>
	  Survival Soup: Although organic canned soup isn't the best option, it's quick and easy if you're in a rush and it is a far superior alternative to fast food. Make sure the soup is packed with meat and veggies rather than white noodles and MSG (harmful additive). If you can make your own homemade bone broth soup to bring, even better!
</p>
<p>
	  ...now get packing!
</p>